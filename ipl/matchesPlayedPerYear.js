const matchesPlayedPerYear = (matches) => {
  const matchesPerYear = matches.reduce((acc, match) => {
    const season = match.season;
    acc[season] = acc[season] ? (acc[season] += 1) : 1;
    return acc;
  },{});
  return matchesPerYear;
};

module.exports = matchesPlayedPerYear;
