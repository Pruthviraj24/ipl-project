const matchesWonByAllTeams = (matches) => {
  const matchesWonByAllTeamsPerYear = matches.reduce((acc, match) => {
    if (acc[match.season]) {
      if (acc[match.season][match.winner]) {
        acc[match.season][match.winner]++;
      } else {
        acc[match.season][match.winner] = 1;
      }
    } else {
      acc[match.season] = {};
      if (acc[match.season][match.winner]) {
        acc[match.season][match.winner]++;
      } else {
        acc[match.season][match.winner] = 1;
      }
    }
    return acc;
  }, {});

  return matchesWonByAllTeamsPerYear;
};
module.exports = matchesWonByAllTeams;
