const matchesWonByMiEachYear = (matchData) => {
  const matchsWonByMi = matchData.filter(
    (eachMatch) => eachMatch.winner === "Mumbai Indians");

  const noOfMatchesWonByMI = matchsWonByMi.reduce((acc,match) => {
    acc[match.season] = acc[match.season] ? (acc[match.season] += 1) : 1;
    return acc;
  },{});

  return noOfMatchesWonByMI;
  
};

module.exports = matchesWonByMiEachYear;
