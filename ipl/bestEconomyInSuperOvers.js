const bestEconomyInSuperOvers = (deliveries) => {
  const ecoOfBowlersRuns = deliveries
    .filter((eachBall) => eachBall.is_super_over !== "0")
    .reduce((acc, curr) => {
      if (acc[curr.bowler]) {
        // const ballCount = Number(curr.ball) <= 6 ? 1 : 0;
        acc[curr.bowler]["runs"] += Number(curr.total_runs);
        acc[curr.bowler]["balls"] += 1;
        acc[curr.bowler]["eco"] = (acc[curr.bowler]["runs"] / (acc[curr.bowler]["balls"] / 6)).toFixed(2);
      } else {
        acc[curr.bowler] = {};
        acc[curr.bowler]["runs"] = Number(curr.total_runs);
        acc[curr.bowler]["balls"] = 1;
      }

      return acc;
    }, {});

  const ecoOfBowler = Object.entries(ecoOfBowlersRuns).reduce((acc, curr) => {
    acc[curr[0]] = curr[1].eco;
    return acc;
  }, {});

  const bestBowler = Object.fromEntries(
    Object.entries(ecoOfBowler)
      .sort(([, a], [, b]) => [, a][1] - [, b][1])
      .slice(0, 1)
  );
  return bestBowler;
};

module.exports = bestEconomyInSuperOvers;
