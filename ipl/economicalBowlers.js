const economicalBowlers = (deliveries, matches) => {
  const matchesId = matches
    .filter((eachMatch) => eachMatch.season === "2015")
    .map((each) => each.id);

  const ecoWtihBallsRuns = deliveries.reduce((acc, curr) => {
    if (matchesId.includes(curr.match_id)) {
      if (acc[curr.bowler]) {
        let ballCount = Number(curr.ball) <= 6 ? 1 : 0;
        acc[curr.bowler]["runs"] += Number(curr.total_runs);
        acc[curr.bowler]["balls"] += ballCount;
        acc[curr.bowler]["eco"] =
          acc[curr.bowler]["runs"] / (acc[curr.bowler]["balls"] / 6);
      } else {
        acc[curr.bowler] = {};
        acc[curr.bowler]["runs"] = Number(curr.total_runs);
        acc[curr.bowler]["balls"] = 1;
      }
    }
    return acc;
  }, {});

  const ecoOfBowlers = Object.entries(ecoWtihBallsRuns).reduce((acc, curr) => {
    acc[curr[0]] = curr[1].eco;
    return acc;
  }, {});

  const result = Object.fromEntries(
    Object.entries(ecoOfBowlers)
      .sort(([, a], [, b]) => [, a][1] - [, b][1])
      .slice(0, 10)
  );

  return result;
};

module.exports = economicalBowlers;
