const highestDismissal = (deliveries) => {
  const batsmanBowler = deliveries.reduce((acc, curr) => {
    if (curr.dismissal_kind !== "" && curr.dismissal_kind !== "run out") {
      if (acc[curr.batsman]) {
        if (acc[curr.batsman][curr.bowler]) {
          acc[curr.batsman][curr.bowler]++;
        } else {
          acc[curr.batsman][curr.bowler] = 1;
        }
      } else {
        acc[curr.batsman] = {};
        if (acc[curr.batsman][curr.bowler]) {
          acc[curr.batsman][curr.bowler]++;
        } else {
          acc[curr.batsman][curr.bowler] = 1;
        }
      }
    }
    return acc;
  }, {});

  let min = 0;

  let batsmanDismissedByBowler = {};
  Object.entries(batsmanBowler).map((eachBatsman) => {
    const dismissalByBowler = Object.entries(eachBatsman[1])
      .sort(([, a], [, b]) => [, b][1] - [, a][1])
      .slice(0, 1);
    if (dismissalByBowler[0][1] > min) {
      min = dismissalByBowler[0][1];
      let temp = {};
      temp[eachBatsman[0]] = dismissalByBowler[0];
      batsmanDismissedByBowler = temp;
    }
  });

  return batsmanDismissedByBowler;
};

module.exports = highestDismissal;
