const wonTossAndMatch = (matches) => {
  const teamsWonTossAndMatch = matches.reduce((acc, match) => {
    if (acc[match.toss_winner]) {
      if (match.toss_winner === match.winner) {
        acc[match.toss_winner] += 1;
      }
    } else {
      if (match.toss_winner === match.winner) {
        acc[match.toss_winner] = 1;
      }
    }
    return acc;
  }, {});

  return teamsWonTossAndMatch;
};

module.exports = wonTossAndMatch;
